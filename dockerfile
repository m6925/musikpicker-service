FROM node:16.13.2-alpine3.14

WORKDIR /workspace
RUN npm install -G axios

COPY package.json /workspace
RUN npm install --only=prod
COPY . /workspace
HEALTHCHECK --interval=12s --timeout=12s --start-period=30s CMD node /workspace/src/healthCheck.js

CMD ["npm", "start"]
