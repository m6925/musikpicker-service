const {startServer} = require('./app/server');
const logger = require('./config/logger');
const dictionary = require('./config/dictionary');

(async () => {
    await run();
})();

async function run() {
    await logger.sendLog(dictionary.service.started);
    startServer();
}
