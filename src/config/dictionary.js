module.exports = {
    service: {
        started: 'Service started',
        stopped: 'Service stopped',
        listening: (port) => {
            return 'Listening on port: '+ port;
        }
    },
    socketIo: {
        userConnected: (socketId) => 'New user connected with socket id: ' + socketId
    },
    errors: {
        validation: {
            vote: 'Body validation failed!'
        },
    }
};
