const dotenv = require('dotenv');
dotenv.config();

module.exports = {
    projectName: process.env.PROJECT_NAME,
    dev: process.env.DEV,
    musicPickerPortNumber: process.env.MUSICPICKER_PORT_NUMBER,
    stateKey: process.env.STATE_KEY,
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    redirectUri: process.env.REDIRECT_URI,
    uiUrl: process.env.UI_URL
};
