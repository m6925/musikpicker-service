const env = require('./env');

module.exports = {
    sendLog: (message) => {
        console.debug('['+new Date().toISOString()+']'+'['+env.projectName+']' + ': ' + message);
    }
};
