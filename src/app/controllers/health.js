module.exports = {
    handleHealthCheck: (req, res) => {
        const healthCheck = {
            uptime: process.uptime(),
            message: 'OK',
            timestamp: Date.now()
        };
        try {
            res.status(200).json(healthCheck);
        } catch (err) {
            healthCheck.message = err.message;
            res.status(503).json(healthCheck);
        }
    }
};
