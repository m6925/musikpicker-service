const config = require('../../config/env');
const {
    setupTokens,
    getAccessToken,
    getRefreshToken,
    isConnected, resetAccessToken, resetRefreshToken
} = require('../services/spotify');
const {resetSuggestion} = require('../services/suggestions');

module.exports = {
    handleLoginRoute: async (req, res) => {
        const scopes = 'user-library-read ' +
            'streaming ' +
            'user-read-playback-position ' +
            'user-read-currently-playing ' +
            'user-read-playback-state ' +
            'user-modify-playback-state '
        ;

        res.redirect('https://accounts.spotify.com/authorize/?' +
            new URLSearchParams({
                response_type: 'code',
                client_id: config.clientId,
                scope: scopes,
                redirect_uri: config.redirectUri,
                state: 'some-state-of-my-choice'
            }).toString()
        );
    },

    handleCallbackRoute: async (req, res) => {
        const code = req.query.code || null;
        await setupTokens(code, true);

        if (getAccessToken() && getRefreshToken()) {
            const io = require('../services/io').get();
            io.emit('loginEvent');
            res.redirect(config.uiUrl);
        } else {
            res.sendStatus(500);
        }
    },

    handleTokenRoute: async (req, res) => {
        res.json({
            loggedIn: isConnected()
        });
    },

    handleDisconnectRoute: async (req, res) => {
        resetAccessToken();
        resetRefreshToken();
        resetSuggestion();
        if (!isConnected()) {
            const io = require('../services/io').get();
            io.emit('logoutEvent');
            res.sendStatus(200);
        } else {
            res.sendStatus(500);
        }
    }
};
