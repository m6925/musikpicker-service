const {SpotifyService, SuggestionService} = require('../services');
const {getUriFromPlaylist, getRandomNumbers} = require('../utils/utils');
const colorArray = require('../utils/colors');

const buildSuggestions = async () => {
    const playlist = await SpotifyService.getCurrentlyPlayingPlaylist();
    if (playlist) {
        const tracks = await SpotifyService.getTracksInPlaylist(
            getUriFromPlaylist(playlist)
        );
        const numbers = getRandomNumbers(tracks.length, 4);
        let tmpSuggestions = [];
        numbers.forEach((number) => {
            tmpSuggestions.push(tracks[number]);
        });

        tmpSuggestions = tmpSuggestions.map((suggestion, index) => {
            return {
                name: suggestion.track.name,
                artist: suggestion.track.artists.map((artist) => {
                    return artist.name;
                }),
                uri: suggestion.track.uri,
                albumArt: suggestion.track.album.images[0].url,
                vote: 0,
                color: colorArray[index]
            };
        });

        tmpSuggestions.forEach((suggestion) => {
            SuggestionService.addSuggestion(suggestion);
        });
    }
};

let previousTrack = {item: {name: 'previousTrack', duration_ms: 0}, progress_ms: 0};
let currentTrack = {item: {name: 'currentTrack', duration_ms: 0}, progress_ms: 0};

module.exports = {
    isConnected: () => !!(SpotifyService.getAccessToken() && SpotifyService.getRefreshToken()),

    refreshSuggestions: async () => {
        SuggestionService.resetSuggestion();
        await buildSuggestions();
    },

    handleCurrentTrackRoute: async (req, res) => {
        const track = await SpotifyService.getCurrentlyPlayingTrack();
        res.status(200).send({
            track: track
        });
    },

    handleSuggestionsRoute: async (req, res) => {
        const tracks = SuggestionService.getSuggestions();
        if (tracks.length === 0) {
            await buildSuggestions();
        }
        res.status(200).send({
            tracks: tracks
        });
    },

    handleRefreshSuggestionsRoute: async (req, res, io) => {
        SuggestionService.resetSuggestion();
        await buildSuggestions();
        io.emit('refreshSuggestions', {
            suggestions: SuggestionService.getSuggestions()
        });
        res.status(200).send();
    },

    handleVoteRoute: async (req, res) => {
        const trackName = req.body.track;
        if (trackName) {
            SuggestionService.voteForSuggetion(trackName);
            res.sendStatus(200);
        } else {
            res.sendStatus(500);
        }
    },

    handleNextMusic: async (io) => {
        currentTrack = await SpotifyService.getCurrentlyPlayingTrack();
        if (currentTrack && currentTrack.progress_ms &&
            currentTrack.item && currentTrack.item.duration_ms &&
            currentTrack.item.name) {
            const timeLeft = currentTrack.item.duration_ms - currentTrack.progress_ms;
            if (timeLeft <= 5000 &&
                previousTrack.item.name !== currentTrack.item.name) {
                console.log('Adding next music to queue');
                let highestVoteTrack = undefined;
                let tmpSuggestions = SuggestionService.getSuggestions();
                if (tmpSuggestions.length === 0) {
                    await buildSuggestions();
                }
                tmpSuggestions.forEach((suggestion) => {
                    if (!highestVoteTrack) {
                        highestVoteTrack = suggestion;
                    } else if (highestVoteTrack.vote < suggestion.vote) {
                        highestVoteTrack = suggestion;
                    }
                });
                await SpotifyService.addTrackToQueue(highestVoteTrack.uri);
                SuggestionService.resetSuggestion();
                setTimeout(async () => {
                    SuggestionService.resetSuggestion();
                    await buildSuggestions();
                    io.emit('playingNewTrack', {
                        track: await SpotifyService.getCurrentlyPlayingTrack(),
                        suggestions: SuggestionService.getSuggestions()
                    });
                }, 8000);
                previousTrack = currentTrack;
            }
        }
    }
};
