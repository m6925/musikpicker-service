const HealthController = require('./health');
const SpotifyController = require('./spotify');
const AuthController = require('./auth');

module.exports = {
    HealthController,
    SpotifyController,
    AuthController
};
