const Joi = require('@hapi/joi');
const dictionary = require('../../config/dictionary');

module.exports = {
    trackVotingValidation: (req, res, next) => {
        const schema = Joi.object().keys({
            track: Joi.string().required()
        });

        const {value, error} = schema.validate(req.body);

        if (error) {
            res.status(400).json({error: dictionary.errors.validation.vote});
        } else {
            req.body = value;
            next();
        }
    }
};
