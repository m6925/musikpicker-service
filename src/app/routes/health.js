const router = require('express').Router();
const HealthController = require('../controllers/health');

router.get('/', HealthController.handleHealthCheck);

module.exports = router;
