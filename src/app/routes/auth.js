const router = require('express').Router();
const {AuthController} = require('../controllers');

router.get('/login', AuthController.handleLoginRoute);

router.get('/callback', AuthController.handleCallbackRoute);

router.get('/token', AuthController.handleTokenRoute);

router.post('/disconnect', AuthController.handleDisconnectRoute);

module.exports = router;
