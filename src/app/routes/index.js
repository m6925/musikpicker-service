const AuthRoutes = require('./auth');
const SpotifyRoutes = require('./spotify');
const HealthRoutes = require('./health');

module.exports = {
    AuthRoutes,
    SpotifyRoutes,
    HealthRoutes
};
