const router = require('express').Router();
const {SpotifyController} = require('../controllers');
const {trackVotingValidation} = require('../validations/index');

router.get('/track/current', SpotifyController.handleCurrentTrackRoute);

router.get('/suggestions', SpotifyController.handleSuggestionsRoute);

router.get('/refreshSuggestions', SpotifyController.handleRefreshSuggestionsRoute);

router.post('/vote', trackVotingValidation, SpotifyController.handleVoteRoute);

module.exports = router;
