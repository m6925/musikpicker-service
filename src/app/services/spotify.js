const config = require('../../config/env');
const axios = require('axios');

let accessToken = undefined;
let refreshToken = undefined;
let tokenExpirationTime = undefined;

module.exports = {
    getAccessToken: () =>{
        return accessToken;
    },

    setAccessToken: (token) => {
        accessToken = token;
    },

    resetAccessToken: () => {
        accessToken = undefined;
    },

    getRefreshToken: () =>{
        return refreshToken;
    },

    setRefreshToken: (token) => {
        refreshToken = token;
    },

    resetRefreshToken: () => {
        refreshToken = undefined;
    },

    isConnected: () => {
        return !!(accessToken && refreshToken);
    },

    getTokenExpirationDate: () => {
        return tokenExpirationTime;
    },

    getCurrentlyPlayingPlaylist: async () => {
        let playlist = undefined;
        try {
            const response = await axios({
                method: 'GET',
                headers: { 'Authorization': 'Bearer ' + accessToken },
                url: 'https://api.spotify.com/v1/me/player'
            });

            if (response.data.context && response.data.context.type === 'playlist') {
                playlist = response.data;
            }
            return playlist;
        } catch (e) {
            return playlist;
        }
    },

    getTracksInPlaylist: async (playlistUri) => {
        let tracks = [];
        try {
            const response = await axios({
                method: 'GET',
                headers: { 'Authorization': 'Bearer ' + accessToken },
                url: 'https://api.spotify.com/v1/playlists/'+playlistUri+'/tracks'
            });

            tracks = response.data.items;
            return tracks;
        } catch (e) {
            return e;
        }
    },

    getCurrentlyPlayingTrack: async () => {
        let track = undefined;
        try {
            const response = await axios({
                method: 'GET',
                headers: { 'Authorization': 'Bearer ' + accessToken },
                url: 'https://api.spotify.com/v1/me/player/currently-playing'
            });

            if (response.data) {
                track = response.data;
            }
            return track;
        } catch (e) {
            return track;
        }
    },

    addTrackToQueue: async (trackUri) => {
        try {
            await axios({
                method: 'POST',
                headers: { 'Authorization': 'Bearer ' + accessToken },
                url: 'https://api.spotify.com/v1/me/player/queue',
                params: {
                    uri: trackUri
                }
            });
        } catch (e) {
            return e;
        }
    },

    /**
     * Refresh the access token with the refresh token.
     * @returns {Promise<void>}
     */
    refreshAccessToken: async function refreshAccessToken() {
        try {
            const response = await axios({
                url: 'https://accounts.spotify.com/api/token',
                method: 'POST',
                params: {
                    refresh_token: refreshToken,
                    grant_type: 'refresh_token'
                },
                headers: {
                    'Authorization': 'Basic ' + (new Buffer(
                        config.clientId + ':' + config.clientSecret
                    ).toString('base64'))
                }
            });

            if (response.data) {
                accessToken = response.data.access_token;
                tokenExpirationTime = Date.now() + response.data.expires_in;
            }
            console.log('Token refreshed');
        } catch (e) {
            console.log(e);
        }
    },

    /**
     * Login to spotify and get the access token and the refresh token.
     * @param code
     * @param autoRefresh
     * @returns {Promise<void>}
     */
    setupTokens: async function setupTokens(code, autoRefresh = false) {
        try {
            const response = await axios({
                url: 'https://accounts.spotify.com/api/token',
                method: 'POST',
                params: {
                    code: code,
                    redirect_uri: config.redirectUri,
                    grant_type: 'authorization_code'
                },
                headers: {
                    'Authorization': 'Basic ' + (new Buffer(
                        config.clientId + ':' + config.clientSecret
                    ).toString('base64')),
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            });

            if (response.data) {
                accessToken = response.data.access_token;
                refreshToken = response.data.refresh_token;
                tokenExpirationTime = Date.now() + response.data.expires_in;

                console.log('Access token: '+accessToken);
                console.log('Refresh token: '+refreshToken);
            }

            if (autoRefresh) {
                console.log('Scheduling auto refresh of the access token every '+
                    response.data.expires_in/2/60 + ' minutes'
                );
                setInterval(async () => {
                    await this.refreshAccessToken();
                }, 1000 * response.data.expires_in/2);
            }
        } catch (e) {
            console.log(e);
        }
    }
};
