const SpotifyService = require('./spotify');
const SuggestionService = require('./suggestions');

module.exports = {
    SpotifyService,
    SuggestionService
};
