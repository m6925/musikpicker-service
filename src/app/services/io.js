const io = require('socket.io');
let socket = undefined;
let initialized = false;

module.exports = {
    init: (httpServer) => {
        initialized = true;
        socket = io(httpServer, {
            cors: {
                origin: '*',
                methods: ['GET', 'POST'],
            },
        });
        return socket;
    },

    get: () => {
        if (initialized) {
            return socket;
        }
        return undefined;
    }
};
