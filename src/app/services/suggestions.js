let suggestions = [];

module.exports = {
    getSuggestions: () => suggestions,

    /**
     * Add a new track as a suggestions.
     * No suggestions can be duplicate.
     * @param newSuggestion
     */
    addSuggestion: (newSuggestion) => {
        if (!suggestions.includes(newSuggestion)) {
            suggestions.push(newSuggestion);
        }
    },

    /**
     * Reset the suggestions to an empty array.
     */
    resetSuggestion: () => {
        suggestions = [];
    },

    /**
     * Vote the a suggestion.
     * @param suggestionName
     */
    voteForSuggetion: (suggestionName) => {
        suggestions.forEach((suggestion) => {
            if (suggestion.name === suggestionName) {
                suggestion.vote++;
            }
        });
    }
};
