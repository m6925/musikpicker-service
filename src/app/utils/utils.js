module.exports = {
    /**
     * Generate X unique random numbers ranging from 0 to maxRange variable.
     * @param maxRange
     * @param size
     * @returns {*[]}
     */
    getRandomNumbers: (maxRange, size) => {
        let arr = [];
        while (arr.length < size) {
            let r = Math.floor(Math.random() * maxRange);
            if (arr.indexOf(r) === -1) { arr.push(r); }
        }
        return arr;
    },

    /**
     *
     * @param playlist
     * @returns {string}
     */
    getUriFromPlaylist: (playlist) => {
        return playlist.context.uri.split(':')[2];
    }
};
