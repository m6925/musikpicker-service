const express = require('express');
const app = express();
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');

app.use(cors({origin: '*'}));
app.use(helmet());
app.use(morgan('short', {}));
app.use(express.json());

const env = require('../config/env');
const logger = require('../config/logger');
const dictionary = require('../config/dictionary');
const {SpotifyController} = require('./controllers');
const spotifyRoutes = require('./routes/spotify');
const authRoutes = require('./routes/auth');
const healthRoutes = require('./routes/health');
const httpServer = require('http').createServer(app);

app.use('/auth', authRoutes);
app.use('/spotify', spotifyRoutes);
app.use('/health', healthRoutes);

const startServer = () => {
    const io = require('./services/io').init(httpServer);
    io.on('connection', (socket) => {
        logger.sendLog(dictionary.socketIo.userConnected(socket.id));
    });

    httpServer.listen(`${env.musicPickerPortNumber}`, async () => {
        await logger.sendLog(dictionary.service.listening(env.musicPickerPortNumber));
    });

    setInterval(async () => {
        if (SpotifyController.isConnected()) {
            await SpotifyController.handleNextMusic(io);
        }
    }, 1000);
};

app.use(async (req, res) => {
    res.status(404).json({error: 'unknown endpoint'});
});

module.exports = {
    app,
    startServer
};
