const axios = require('axios');

axios.get('http://localhost:7878/health', {timeout: 5000})
    .then((res) => {
        if (res.status === 200) {
            process.exit(0);
        }
        process.exit(1);
    })
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
