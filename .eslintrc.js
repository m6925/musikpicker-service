module.exports = {
    'env': {
        'commonjs': true,
        'es2021': true,
        'mocha': true,
        'amd': false,
        'node': true
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaVersion': 12
    },
    'globals': {
        'sinon': true,
        'expect': true,
        'module': true
    },
    'rules': {
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'eol-last': [
            'error',
            'always'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'eqeqeq': [
            'error',
            'allow-null'
        ],
        'func-call-spacing': [
            'error',
            'never'
        ],
        'comma-spacing': [
            'error',
            {
                'before': false,
                'after': true
            }
        ],
        'space-before-blocks': [
            'error',
            'always'
        ],
        'space-in-parens': [
            'error',
            'never'
        ],
        'key-spacing': [
            'error',
            {
                'beforeColon': false,
                'afterColon': true
            }
        ],
        'keyword-spacing': [
            'error',
            {
                'before': true,
                'after': true
            }
        ],
        'curly': [
            'error'
        ],
        'max-len': [
            'error',
            {
                'code': 100
            }
        ],
        'block-spacing': [
            'error',
            'always'
        ]
    }
};
